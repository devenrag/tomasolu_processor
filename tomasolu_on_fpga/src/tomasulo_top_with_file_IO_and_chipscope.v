//-----------------------------------------------------------------------------
 // File name: tomasulo_top_with_file_IO_and_chipscope.v
 // Create/rivision Date:    07/21/2014
 // Module Name:    tomasulo_top_with_file_IO_and_chipscope
 // Function : Top file for the Tomasulo CPU project with file i/o and chipscope (old top file: top_cpu_slow_r1.vhd)
 // Author:         Jizhe Zhang

 // This file is written by Jizhe Zhang on July 16, 2014 incorporating UART based file I/O
 // It is based on the earlier top file top_cpu_slow_r1.vhd by Prasanjeet Das which used Epp-based file I/O
 // The UART code is based on Xilinx's Programmable Wave Generator module wave_gen.v
 // Chipscope was added by Jizhe Zhang and Abhinay Kayastha.
 // The PC-side UART software was designed by Fangzhou Wang.
//-----------------------------------------------------------------------------


`timescale 1ns/1ps


module tomasulo_top_with_file_IO_and_chipscope (
  input            clk_pin,      // Clock input (from pin)
  input            rst_pin,        // Active HIGH reset (from pin)

  // RS232 signals
  input            rxd_pin,        // RS232 RXD pin
  output           txd_pin,        // RS232 RXD pin

  // button L
  input             BTNL,
  //input             BTNR,

  // LED outputs
  output     [7:0] led_pins,         // 8 LED outputs
  
  // Walking LED 
  output     [7:0] walking_led       //display the walking led
);


//***************************************************************************
// Parameter definitions
//***************************************************************************

  parameter BAUD_RATE           = 115_200;   

  parameter CLOCK_RATE_RX       = 25_000_000;
  parameter CLOCK_RATE_TX       = 25_000_000;
  
  localparam
      ACK = 8'h06,
      SOH = 8'h01,
      EOT = 8'h03,
      SOT = 8'h02,
      EOM = 8'h19,
      CR = 8'h0d,
      LF = 8'h0a,
      EOF = 8'h04; 

//***************************************************************************
// Reg declarations
//***************************************************************************

//***************************************************************************
// Wire declarations
//***************************************************************************

  // To/From IBUFG/OBUFG
  wire 		  BCLK;
  wire		  quarter_clk;
  wire 		  clk_i;
  wire        rst_i;          
  wire        rxd_i;         
  (* mark_debug = "true"*) wire        txd_o;
  wire [7:0]  led_o;

  // From Clock Generator
  wire        clk_sys;         // Receive clock
  wire        clk_smpl;		   // Sample clock
  reg [31:0]  divclk;         // Clock divider

  // From Reset Generator
  wire        rst_clk;     // Reset, synchronized to clk_rx

  // From the RS232 receiver
  wire        rxd_clk_rx;     // RXD signal synchronized to clk_rx
  wire        rx_data_rdy;    // New character is ready
  wire        rx_data_rdy_reg;
  wire [7:0]  rx_data;        // New character
  wire        rx_lost_data;   // Rx data lost

  // From the debouncer module
  wire        btnl_scen;        // SCEN of btnl
  // wire        btnr_scen;
  // From the UART transmitter
  wire        tx_fifo_full;  // Pop signal to the char FIFO
  
  // From the sending file module
  wire          send_fifo_full;     // the input fifo of this module is full
  wire          send_finish;        // the sending process has finished; 1 clock wide
  wire [7:0]    tx_din_send_module; // data to the tx module
  wire          tx_write_en_send_module;        // write enable signal to the tx module
  wire          rx_read_en_send_module;         // pop signal to the rx module
  
  // From the receiving file module
  wire          rx_read_en_receive_module;
  wire [7:0]    tx_din_receive_module;
  wire          tx_write_en_receive_module;
  wire [7:0]    receive_fifo_dout;          // the received data flow
  wire          receive_data_rdy;           // received data ready
  wire [7:0]    reg_addr;                   // reg address
  wire [7:0]    reg_pointer;                // reg pointer content
  wire          reg_ready;                  // reg information ready signal
    
  // Given in the current module
  // to the tx module  
  wire [7:0]    tx_din;     // data to be sent in tx
  wire          tx_write_en;    // send signal to tx
  wire          tx_store_qual;// storage qualification
  wire [1:0]    tx_frame_indicator; //frame indicator
  wire          tx_bit_indicator; //bit indicator
  wire [7:0]    char_fifo_dout; //registered received data for chipscope
  // to the rx module
  wire          rx_read_en;     // pop an entry in the rx fifo
  wire          rx_store_qual; // storage qualification
  wire [1:0]    rx_frame_indicator; //frame indicator
  wire          rx_bit_indicator; //bit indicator
  wire [7:0]    rx_data_reg; //registered received data for chipscope
  // to the send file module
  reg [7:0]     send_fifo_din;      // the input data to the send module
  wire          send_fifo_we;       // signal to send data
  wire          start_send_file;         // start send signal
  // to the receive file module
  wire          receive_fifo_re;        // pop information from receive file module
  // decide if current is rx or tx
  wire           receive_sendbar;       // 1 means currently is receiving data; 0 mean currently is sending data

  // data for the Tomasulo project
  wire          toma_reset;
  wire          toma_test_mode;
  // Instruction memory
  wire [5:0]    toma_addr_in;
  reg [127:0]   toma_data_in;
  reg           toma_IM_wea;
  wire [127:0]  toma_IM_data_out;
  //Data memory
  reg           toma_DM_wea;
  wire [31:0]   toma_DM_data_out;
  reg  [5:0]	toma_addr_in_reg;

  // for debug
  // for chipscope  -- added by Abhinay
  wire [35:0]    control_bus;
  wire [35:0]    control_bus_test;
  wire [23:0]    data;
  wire [69:0]    data_test;
  wire 			 word_indicator;
  reg            data_rd, data_wr;
  reg [3:0]		 Inst_stream_cnt;
  
  // Added by Srikar Matam for Chipscope
  wire [35:0]	 control_bus_inst;
  wire [31:0]    toma_StoreData_PhyReg;		
  wire [31:0]    toma_SwAddr_Rob;			
  wire 			 toma_CommitMemWrite_Rob;	
  wire [4:0]     toma_CommitRdAddr_Rob;		
  wire			 toma_CommitRegWrite_Rob;
  wire [31:0]	 toma_CommitRegData_Rob;
  wire           toma_Commit_ROB;
  wire [103:0]	 data_test_inst;
  
  // walking leds
  reg [7:0]		walking_led_reg;
  wire 			clk_led;
  wire 			walking_led_en;
  
//***************************************************************************
// Code
//***************************************************************************

  // Instantiate input/output buffers
  IBUF IBUF_rst_i0      (.I (rst_pin),      .O (rst_i));
  IBUF IBUF_rxd_i0      (.I (rxd_pin),      .O (rxd_i));
  IBUF IBUF_clk			(.I (clk_pin), 	  .O (clk_i));

  OBUF OBUF_txd         (.I(txd_o),         .O(txd_pin));
  OBUF OBUF_led_i0      (.I(led_o[0]),      .O(led_pins[0]));
  OBUF OBUF_led_i1      (.I(led_o[1]),      .O(led_pins[1]));
  OBUF OBUF_led_i2      (.I(led_o[2]),      .O(led_pins[2]));
  OBUF OBUF_led_i3      (.I(led_o[3]),      .O(led_pins[3]));
  OBUF OBUF_led_i4      (.I(led_o[4]),      .O(led_pins[4]));
  OBUF OBUF_led_i5      (.I(led_o[5]),      .O(led_pins[5]));
  OBUF OBUF_led_i6      (.I(led_o[6]),      .O(led_pins[6]));
  OBUF OBUF_led_i7      (.I(led_o[7]),      .O(led_pins[7]));
  // this follows the Nexys3 setting: the clock frequency for the Tomasulo
  // module is 25MHz. This clock serves as the top clock signal.
  BUFG IBUFG_CLK       (.I(quarter_clk),      .O(BCLK));

  
  assign clk_sys = BCLK;
  assign quarter_clk = divclk[1];
  
  // added by Abhinay
  // Sample clock generation
  
  always @ (posedge clk_i)
  begin
	if (rst_i)
    begin
		divclk <= 6'd0;
	end
	else
	begin
		divclk <= divclk + 1'b1;
	end
  end
  
  assign clk_smpl = divclk[6];  
  
  // Walking Led
  assign clk_led = divclk[23];
  
  //Modified by Srikar Matam
  always @ (posedge clk_led)
  begin
	if (rst_i)
    begin
		walking_led_reg <= 8'b00000000;
	end
	else
	begin
		if(walking_led_en == 1'b1)
		begin
			case(walking_led_reg)				// For students' code, the walking leds will run in opposite direction comapred to TA's code
				8'b00000000: walking_led_reg <= 8'b00000001;
				8'b00000001: walking_led_reg <= 8'b00000010;
				8'b00000010: walking_led_reg <= 8'b00000100;
				8'b00000100: walking_led_reg <= 8'b00001000;
				8'b00001000: walking_led_reg <= 8'b00010000;
				8'b00010000: walking_led_reg <= 8'b00100000;
				8'b00100000: walking_led_reg <= 8'b01000000;
				8'b01000000: walking_led_reg <= 8'b10000000;
				8'b10000000: walking_led_reg <= 8'b00000001;
			endcase
		end
		else
		begin
			walking_led_reg <= 8'b00000000;
		end
	end
  end

  assign walking_led = walking_led_reg;		       
  
  // ended by Srikar Matam
	
    // This function takes the lower 7 bits of a character and converts them
    // to a hex digit. It returns 5 bits - the upper bit is set if the character
    // is not a valid hex digit (i.e. is not 0-9,a-f, A-F), and the remaining
    // 4 bits are the digit
    function [4:0] to_val;
        input [6:0] char;
        begin
            if ((char >= 7'h30) && (char <= 7'h39)) // 0-9
            begin
                to_val[4]   = 1'b0;
                to_val[3:0] = char[3:0];
            end
            else if (((char >= 7'h41) && (char <= 7'h46)) || // A-F
                ((char >= 7'h61) && (char <= 7'h66)) )  // a-f
            begin
                to_val[4]   = 1'b0;
                to_val[3:0] = char[3:0] + 4'h9; // gives 10 - 15
            end
            else 
            begin
                to_val      = 5'b1_0000;
            end
        end
    endfunction
    
    function [7:0] to_char;
        input [3:0] val;
        begin
            if ((val >= 4'h0) && (val <= 4'h9)) // 0-9
            begin
                to_char = {4'h3, val};
            end
            else
            begin
                to_char = val + 8'h37; // gives 10 - 15
            end
        end
    endfunction

// Instantiate the ICON
icon_tomasulo icon_1(
	.CONTROL0(control_bus),
	.CONTROL1(control_bus_test),
	.CONTROL2(control_bus_inst)
	);
	
//data for ila_1
assign data[7:0] = rx_data_reg;
assign data[15:8] = char_fifo_dout;
assign data[17:16] = rx_frame_indicator;
assign data[19:18] = tx_frame_indicator;
assign data[20] = rx_bit_indicator;
assign data[21] = tx_bit_indicator;
assign data[22] = rxd_i;
assign data[23] = txd_o;

//data and trigger for ila_2
assign data_test[31:0] = toma_data_in[31:0];
assign data_test[63:32] = toma_DM_data_out;
assign data_test[64] = data_wr;
assign data_test[65] = data_rd;
assign data_test[69:66] = Inst_stream_cnt;
assign word_indicator = 1'b1 ? (data_rd == 1'b1 || data_wr == 1'b1) : 1'b0;

//data and trigger for ila_3	- Added by Srikar Matam
assign data_test_inst[0] 		= toma_Commit_ROB;
assign data_test_inst[32:1] 	= toma_SwAddr_Rob;
assign data_test_inst[33]		= toma_CommitMemWrite_Rob;
assign data_test_inst[65:34]	= toma_StoreData_PhyReg;
assign data_test_inst[70:66]	= toma_CommitRdAddr_Rob;
assign data_test_inst[71]		= toma_CommitRegWrite_Rob;
assign data_test_inst[103:72]	= toma_CommitRegData_Rob;

// Instantiate the ILA
ila_tomasulo ila_1 (
	 .CONTROL(control_bus),
	 .CLK(clk_smpl),
	 .DATA(data),
	 .TRIG0(rx_data_rdy_reg),
	 .TRIG1(rx_data_reg),
	 .TRIG2(rx_store_qual),
	 .TRIG3(tx_store_qual)
	 );	
	
ila_test ila_2 (
	.CONTROL(control_bus_test),
	.CLK(clk_sys),
	.DATA(data_test),
	.TRIG0(rx_data_reg),
	.TRIG1(rx_data_rdy_reg),
	.TRIG2(word_indicator),
	.TRIG3(Inst_stream_cnt)
	);		

//Added by Srikar Matam
ila_inst ila_3 (
	.CONTROL(control_bus_inst),
	.CLK(clk_sys),
	.DATA(data_test_inst),
	.TRIG0(toma_Commit_ROB),
	.TRIG1(toma_CommitRegWrite_Rob),
	.TRIG2(toma_CommitMemWrite_Rob)
	);
	
  // Instantiate the reset generator
  rst_gen rst_gen_i0 (
    .clk_i          (clk_sys),          // Receive clock
    .rst_i           (rst_i),           // Asynchronous input - from IBUF
    .rst_o      (rst_clk)      // Reset, synchronized to clk_rx
  );
  
/*  // Debouncing module for BTNL
  ee201_debouncer debouncer_i0(
      .CLK          (clk_sys), 
      .RESET        (rst_clk), 
      .PB           (BTNL), 
      .DPB          (), 
      .SCEN         (btnl_scen),
      .MCEN         (), 
      .CCEN         ()
  );*/
  
/*    // Debouncing module for BTNR
  ee201_debouncer debouncer_i1(
      .CLK          (clk_sys), 
      .RESET        (rst_clk), 
      .PB           (BTNR), 
      .DPB          (), 
      .SCEN         (btnr_scen),
      .MCEN         (), 
      .CCEN         ()
  );*/
 
  // the tomasulo module
  tomasulo_top cpu_2_inst (
  // toma_test cpu_2_inst (
      .Clk                      (clk_sys),
      .Reset                    (toma_reset),
      .fio_icache_addr_IM       (toma_addr_in_reg),
      .fio_icache_data_in_IM    (toma_data_in),
      .fio_icache_wea_IM        (toma_IM_wea),
      .fio_icache_data_out_IM   (toma_IM_data_out),
      .fio_icache_ena_IM        (1'b1),
      .fio_dmem_addr_DM         (toma_addr_in_reg),
      .fio_dmem_data_out_DM     (toma_DM_data_out),
      .fio_dmem_data_in_DM      (toma_data_in[31:0]),
      .fio_dmem_wea_DM          (toma_DM_wea),
      .Test_mode                (toma_test_mode),
      .walking_led_start        (walking_led_en),
	  .StoreData_PhyReg			(toma_StoreData_PhyReg),
	  .SwAddr_Rob				(toma_SwAddr_Rob),
	  .CommitMemWrite_Rob		(toma_CommitMemWrite_Rob),
	  .CommitRdAddr_Rob			(toma_CommitRdAddr_Rob),
	  .CommitRegWrite_Rob		(toma_CommitRegWrite_Rob),
	  .CommitRegData_Rob		(toma_CommitRegData_Rob),
	  .Commit_ROB				(toma_Commit_ROB)
  );
    
  // file sending module
  send_file send_file_i0 (
      // for debug
      .state                (),
      // clock and reset
      .clk                  (clk_sys),
      .reset                (rst_clk),
      // communication with user
      .start_send           (start_send_file),     // start signal
      .send_fifo_din        (send_fifo_din),                // data input to the input fifo
      .send_fifo_we         (send_fifo_we),                 //write enable to the input fifo
      .send_fifo_full       (send_fifo_full),              // input fifo is full
      .finish_send          (send_finish),        // send finishes
      
      // communication with tx fifo
      .tx_fifo_full         (tx_fifo_full),           // tx fifo is full
      .tx_fifo_din          (tx_din_send_module),  // send data to the tx fifo
      .tx_fifo_we           (tx_write_en_send_module),           // write enable to tx fifo
      
      // communication with rx module
      .rx_data              (rx_data),  // Character to be parsed
      .rx_data_rdy          (rx_data_rdy), // Ready signal for rx_data
      .rx_read_en           (rx_read_en_send_module) // Pop entry from rx fifo
  );
  
  // file receiving module
  receive_file receive_file_i0(
      .state                (),     //for debug
      // clk and reset
      .clk                  (clk_sys),         // Clock input
      .reset                (rst_clk),     // Active HIGH reset - synchronous to clk_rx
      
      // communication with rx module
      .rx_data              (rx_data),        // Character to be parsed
      .rx_data_rdy          (rx_data_rdy),         // Ready signal for rx_data
      .rx_read_en           (rx_read_en_receive_module),       // pop entry in rx fifo
      
      // communication with tx module
      .tx_fifo_full         (tx_fifo_full),     // the tx fifo is full
      .tx_din               (tx_din_receive_module),         // data to be sent
      .tx_write_en          (tx_write_en_receive_module),          // write enable to tx         
      
      // communication with user
      .receive_fifo_dout    (receive_fifo_dout),      // data output of the fifo
      .receive_data_rdy     (receive_data_rdy),         // there is data ready at the output fifo
      .receive_fifo_re      (receive_fifo_re),            // pop entry in the receive fifo
      
      // register information
      .reg_addr             (reg_addr),       // register address of the specifier
      .reg_pointer          (reg_pointer),    // content of the specifier
      .reg_ready            (reg_ready)      // indication when the reg data is available. It's available only 
                                          // after both the address and the content is ready.
      );
      
      
  // Instantiate the UART receiver
  uart_rx #(
    .BAUD_RATE   (BAUD_RATE),
    .CLOCK_RATE  (CLOCK_RATE_RX)
  ) uart_rx_i0 (
  //system configuration:
    .clk_rx      (clk_sys),              // Receive clock
    .rst_clk_rx  (rst_clk),          // Reset, synchronized to clk_rx 
    .rxd_i       (rxd_i),               // RS232 receive pin
    .rxd_clk_rx  (rxd_clk_rx),          // RXD pin after sync to clk_rx    
    
  //user interface:
    .read_en     (rx_read_en),                    // input to the module: pop an element from the internal fifo
    .rx_data_rdy (rx_data_rdy),         // New data is ready
	 .rx_data_rdy_reg (rx_data_rdy_reg),
    .rx_data     (rx_data),             // New data
    .lost_data   (rx_lost_data),                    // fifo is full but new data still comes in, resulting in data lost
    .frm_err     (),                     // Framing error (unused)
	.rx_store_qual      (rx_store_qual),
    .rx_frame_indicator (rx_frame_indicator), 
    .rx_bit_indicator   (rx_bit_indicator),
    .rx_data_reg        (rx_data_reg)
	 
  );

  // Instantiate the UART transmitter
  uart_tx #(
    .BAUD_RATE    (BAUD_RATE),
    .CLOCK_RATE   (CLOCK_RATE_TX)
  ) uart_tx_i0 (
  //system configuration:
    .clk_tx             (clk_sys),          // Clock input
    .rst_clk_tx         (rst_clk),      // Reset - synchronous to clk_tx
    .txd_tx             (txd_o),           // The transmit serial signal

  //user interface:
    .write_en           (tx_write_en), // signal to send to data out
    .tx_din             (tx_din), // data to be sent
    .tx_fifo_full       (tx_fifo_full),  // the internal fifo is full, should stop sending data
	.tx_store_qual      (tx_store_qual),
    .tx_frame_indicator (tx_frame_indicator), 
    .tx_bit_indicator   (tx_bit_indicator),
    .char_fifo_dout     (char_fifo_dout)
  );
  
  // mux for input to rx and tx module
  assign tx_din = receive_sendbar ? tx_din_receive_module : tx_din_send_module;
  assign tx_write_en = receive_sendbar ? tx_write_en_receive_module : tx_write_en_send_module;
  assign rx_read_en = receive_sendbar ? rx_read_en_receive_module : rx_read_en_send_module;
  

  // the states for the big state machine
  localparam
    IDLE = 4'b0000,
    WAIT_RECV_DM = 4'b0001,     // wait for the SOT signal for X
    WAIT_RECV_IM = 4'b0010,         // wait for the SOT signal for Y
    RECV_DM = 4'b0011,
    RECV_IM = 4'b0100,
    RUN = 4'b0101,
    SEND_DM = 4'b0110,
    SEND_IM = 4'b0111,
	WAIT_SEND_IM_DONE = 4'b1000,
    DONE = 4'b1001;

  localparam
    SEND_IDLE = 4'b0000,
    SEND_WAIT = 4'b0001,
    SEND_DATA = 4'b0010,
    SEND_DONE = 4'b0011,
    SEND_START = 4'b0100,
    SEND_LFCR = 4'b0101,
    SEND_EOF  = 4'b0110;
    
  reg [3:0]         state;
  reg [3:0]         state_send;
  reg [6:0]         cnt_row;       // 64 rows for IM and DM
  reg [4:0]         cnt_nibble;    // 32 nibbles for IM, 8 nibbles for DM
  reg [39:0]        cnt_wait;       // 1024 clocks for waiting when running Tomasulo module
  reg [2:0]         cnt_send_wait;  // the counter for the SEND_WAIT state
  reg [6:0]         cnt_send_row;   // the counter for the number of rows during the sending process
  reg [4:0]         cnt_send_character;         // the counter for the special characters (SOH, SOT, EOT, IM/DM, /n, /t, ...)
  reg [4:0]         cnt_send_nibble;
  reg               cnt_send_LFCR;
  wire [4:0]        char_to_digit = to_val(receive_fifo_dout[6:0]);      // the hex result of the received data
  wire              done = (state == DONE);
  
  // for testing
  wire [3:0] temp = toma_DM_data_out[3:0];
  // assign led_o[0] = rx_lost_data; 
  // assign led_o[1] = receive_sendbar;
  // assign led_o[2] = send_fifo_full;
  // assign led_o[3] = done;
  assign led_o[3:0] = cnt_row[3:0];
  assign led_o[7:4] = temp;
  //assign led_o[5:4] = reg_pointer;
  //assign led_o[7:4] = state_temp;
  
  // combinational logic
  assign toma_addr_in = (state == IDLE || state == WAIT_RECV_IM || state == WAIT_RECV_IM || state == RECV_DM || state == RECV_IM) ? 
                          cnt_row[5:0]: cnt_send_row[5:0];
  assign receive_fifo_re = (state == WAIT_RECV_IM || state == WAIT_RECV_DM || state == RECV_DM || state == RECV_IM) && receive_data_rdy;
  assign start_send_file = (state == SEND_IM || state == SEND_DM);

  assign send_fifo_we = (state == SEND_IM || state == SEND_DM) && (~send_fifo_full) &&
      (state_send == SEND_START || state_send == SEND_LFCR || state_send == SEND_EOF || state_send == SEND_DATA);   
  assign receive_sendbar = (state == IDLE || state == WAIT_RECV_IM || state == WAIT_RECV_DM || 
										state == RECV_IM || state == RECV_DM);
  // the toma_reset and toma_test_mode signals
  // The toma_test_mode is active HIGH
  // The toma_reset is active HIGH
  assign toma_test_mode = (state != RUN);   // load the data whenever the state is not running
  assign toma_reset = (state != RUN);       // The Tomasulo module is running during the RUN state
  
  // the logic for input to the send buffer. This is the entire character flow of sending a file, including controling characters
  always @ (*)
  begin
      case (state_send)
          SEND_START:
          begin
              if (cnt_send_character == 0) send_fifo_din = SOH;
              else if (cnt_send_character == 1) send_fifo_din = (state == SEND_IM) ? "I" : "D";
              else if (cnt_send_character == 2) send_fifo_din = "M";
              else if (cnt_send_character == 3) send_fifo_din = EOT;
              else if (cnt_send_character == 4) send_fifo_din = SOT;
              else send_fifo_din = 8'hxx;
          end
          SEND_EOF:
          begin
              send_fifo_din = EOF;
          end
          SEND_DATA:
          begin
              if (state == SEND_DM)
                  send_fifo_din = to_char(toma_DM_data_out[(7'd31-({2'b00,cnt_send_nibble}<<2)) -: 4]);
              else //if (state == SEND_IM)
                  send_fifo_din = to_char(toma_IM_data_out[(7'd127-({2'b00,cnt_send_nibble}<<2)) -: 4]);
          end
          SEND_LFCR:
          begin
              if (cnt_send_LFCR == 0)
                  send_fifo_din = 8'h0d;
              else //if(cnt_send_LFCR == 1)
                  send_fifo_din = 8'h0a;
          end
          default: send_fifo_din = 8'hxx;
      endcase
  end

  // logic for the write enable signal for the IM and DM
  always @ (posedge clk_sys)
  begin
      if (rst_clk)
      begin
          toma_IM_wea <= 0;
          toma_DM_wea <= 0;
		  data_wr <=0;
      end
	  else
		  toma_addr_in_reg <= toma_addr_in;		// This is tricky. 
		  begin
		  // IM write enable
		  if (toma_IM_wea == 1) 
		  begin
			  toma_IM_wea <= 0;     // high for only 1 clk
		  end
		  else
		  begin
			  if (state == RECV_IM && cnt_nibble == 5'd31 && receive_data_rdy && (~char_to_digit[4]))
				  toma_IM_wea <= 1;
		  end
		  // DM write enable
		  if (toma_DM_wea == 1) 
		  begin
			  toma_DM_wea <= 0;     // high for only 1 clk
			  data_wr <= 0;
		  end
		  else
		  begin
			  if (state == RECV_DM && cnt_nibble == 5'd7 && receive_data_rdy && (~char_to_digit[4]))
			  begin
				  toma_DM_wea <= 1; 		// after this clock, data is ready for the input to the BRAM
				  data_wr <= 1;
			  end
		  end
	  end
  end
  
  // the SM for sending IM and DM data

  always @ (posedge clk_sys)
  begin
      if (rst_clk)
      begin
        state_send <= SEND_IDLE;
        cnt_send_wait <= 0;
		data_rd <= 0;
		Inst_stream_cnt <= 1; // Start with inst #1
      end
      else
      begin
	      if (data_rd == 1'b1)
		  begin
			data_rd <= 0;
		  end
          case (state_send)
          SEND_IDLE:
          begin
              cnt_send_row <= 0;
              cnt_send_wait <= 3'd2;
              cnt_send_character <= 0;
              cnt_send_nibble <= 0;
              cnt_send_LFCR <= 0;
              if (state == SEND_IM || state == SEND_DM)
                  state_send <= SEND_START;
          end
          SEND_START:
          begin
              // send the SOH, IM/DM, EOT, SOT signals/characters
              if (~send_fifo_full)
              begin
                  cnt_send_character <= cnt_send_character + 1'b1;
                  if (cnt_send_character == 4)
                      state_send <= SEND_DATA;
              end

          end
          SEND_WAIT:
          begin
              // waiting for 3 clock cycles (3 for safe, 2 should suffice: one for updating cnt_row 
              // and one for BRAM) for updating of the data out side
              cnt_send_wait <= cnt_send_wait - 1'b1;
              if (cnt_send_wait == 0) 
              begin
                  cnt_send_nibble <= 0;
                  state_send <= SEND_DATA;
              end
          end
          SEND_DATA:
          begin
             if (~send_fifo_full) 
             begin
                 cnt_send_nibble <= cnt_send_nibble + 1'b1;
                 if ((state == SEND_IM && cnt_send_nibble == 5'd31) 
                     || (state == SEND_DM && cnt_send_nibble == 5'd7))
                 begin
                     // next line: first LFCR, while reading in new line
                     cnt_send_row <= cnt_send_row + 1'b1;
                     cnt_send_LFCR <= 0;
                     state_send <= SEND_LFCR;
					 if (state == SEND_DM)
						  data_rd <= 1;
                 end
             end
          end
          SEND_LFCR:
          begin				  
              if (~send_fifo_full)
              begin
                  cnt_send_LFCR <= cnt_send_LFCR + 1'b1;
                  if (cnt_send_LFCR == 2'd1)
                      // tricky: even though it's a BRAM, we are actually spending 
                  // 2 clock cycles in the SEND_LFCR state, in which the content
                  // in the BRAM have been read out. Hence we don't need to explicitly wait for
                  // the BRAM data
                  begin
                      state_send <= SEND_DATA;
                      cnt_send_nibble <= 0;
                     if (cnt_send_row == 64)
                     begin
                         // in SEND_EOF cnt_send_character==5
                         state_send <= SEND_EOF;
                     end
                  end
              end
          end
          SEND_EOF:
              if (~send_fifo_full)
                  state_send <= SEND_DONE;
          SEND_DONE:
		    begin
			      // instruction stream count
				  if (state == SEND_DM)
					  Inst_stream_cnt <= Inst_stream_cnt + 1'b1;	
					  
				  state_send <= SEND_IDLE;
		    end	  
          endcase
      end
  end
  
  always @ (posedge clk_sys)
  begin
    if (rst_clk)
    begin
        state <= IDLE;
        cnt_nibble <= 5'bxxxxx;
    end
    else
    case (state)
    IDLE:           // waiting for the reg ready signal; do not read incoming stream
    begin
        if (reg_ready && reg_addr == 8'h00 && reg_pointer == 8'h00)
            state <= WAIT_RECV_DM;
        else if (reg_ready && reg_addr == 8'h01 && reg_pointer == 8'h00)
            state <= WAIT_RECV_IM;

        cnt_row <= 7'd0;
        cnt_nibble <= 0;
    end
    WAIT_RECV_DM :
        if (receive_data_rdy && receive_fifo_dout == SOT)       // ignore any non-SOT characters
        begin
            state <= RECV_DM;
            cnt_nibble <= 0;
            cnt_row <= 0;
        end
    WAIT_RECV_IM:
        if (receive_data_rdy && receive_fifo_dout == SOT)       // ignore any non-SOT characters
        begin
            state <= RECV_IM;
            cnt_nibble <= 0;
            cnt_row <= 0;
        end
    RECV_DM :
    begin
        if (receive_data_rdy && (~char_to_digit[4]))        // ignore any non-numbers : include \n and \r
        begin
            toma_data_in[(7'd31-({2'b00,cnt_nibble}<<2)) -: 4] <= char_to_digit[3:0];
			// toma_data_in[(7'd31-({2'b00,cnt_nibble}<<2)) -: 4] <= cnt_row[3:0];
            cnt_nibble <= cnt_nibble+1'b1;
            if (cnt_nibble == 5'd7)         // we have only 8 nibbles (32 bit) per DM
				 begin
					  cnt_row <= cnt_row + 1'b1;
					  cnt_nibble <= 0;
				 end
            if (cnt_row == 7'd63 && cnt_nibble == 5'd7) 
				 begin
					  cnt_row <= 0;
					  cnt_nibble <= 0;
					  state <= IDLE;
				 end
        end
    end   
    RECV_IM:
        if (receive_data_rdy && (~char_to_digit[4]))        // ignore any non-numbers : include \n and \r
        begin
            toma_data_in[(7'd127-({2'b00,cnt_nibble}<<2)) -: 4] <= char_to_digit[3:0];
			// toma_data_in[(7'd127-{2'b00,cnt_nibble}<<2) -: 4] <= cnt_nibble[3:0];
            cnt_nibble <= cnt_nibble+1'b1;
            if (cnt_nibble == 5'd31)            // we have only 32 nibbles (128 bit) per IM
				 begin
					  cnt_row <= cnt_row + 1'b1;
					  cnt_nibble <= 0;
				 end
            if (cnt_row == 6'd63 && cnt_nibble == 5'd31) 
				 begin
					  cnt_row <= 0;
					  cnt_nibble <= 0;
					  state <= RUN;       // after receiving the IM start running the Tomasulo module
					  cnt_wait <= 40'd100000000;
				 end
        end
    RUN:            // Enter the div state: start running the sub state machine for division
    begin
    /// TODO: balabala
        cnt_wait <= cnt_wait - 1'b1;
        if (cnt_wait == 0)
        begin
            state <= SEND_DM;
        end
    end
    SEND_DM:
        if (state_send == SEND_DONE)
            state <= SEND_IM;
    SEND_IM:
        if (state_send == SEND_DONE)
            state <= WAIT_SEND_IM_DONE;
	WAIT_SEND_IM_DONE:					// Wait for the entire sending process to finish; 
												// After that go back to the receive mode (IDLE state)
			if (send_finish == 1'b1) state <= IDLE;
    DONE:       // go back to IDLE
    begin
        state <= IDLE;
    end
    endcase
  end


endmodule
