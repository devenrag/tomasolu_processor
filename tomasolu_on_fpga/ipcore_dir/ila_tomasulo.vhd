-------------------------------------------------------------------------------
-- Copyright (c) 2017 Xilinx, Inc.
-- All Rights Reserved
-------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor     : Xilinx
-- \   \   \/     Version    : 14.7
--  \   \         Application: XILINX CORE Generator
--  /   /         Filename   : ila_tomasulo.vhd
-- /___/   /\     Timestamp  : Sat Jul 08 18:34:02 Pacific Daylight Time 2017
-- \   \  /  \
--  \___\/\___\
--
-- Design Name: VHDL Synthesis Wrapper
-------------------------------------------------------------------------------
-- This wrapper is used to integrate with Project Navigator and PlanAhead

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
ENTITY ila_tomasulo IS
  port (
    CONTROL: inout std_logic_vector(35 downto 0);
    CLK: in std_logic;
    DATA: in std_logic_vector(23 downto 0);
    TRIG0: in std_logic_vector(0 to 0);
    TRIG1: in std_logic_vector(7 downto 0);
    TRIG2: in std_logic_vector(0 to 0);
    TRIG3: in std_logic_vector(0 to 0));
END ila_tomasulo;

ARCHITECTURE ila_tomasulo_a OF ila_tomasulo IS
BEGIN

END ila_tomasulo_a;
