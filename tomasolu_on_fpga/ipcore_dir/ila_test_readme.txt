The following files were generated for 'ila_test' in directory
C:\Xilinx_projects\tomasolu\tomasolu\ipcore_dir\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * ila_test.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * ila_test.cdc
   * ila_test.constraints/ila_test.ucf
   * ila_test.constraints/ila_test.xdc
   * ila_test.ncf
   * ila_test.ngc
   * ila_test.ucf
   * ila_test.vhd
   * ila_test.vho
   * ila_test.xdc
   * ila_test_xmdf.tcl

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * ila_test.vho

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * ila_test.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * ila_test.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * _xmsgs/pn_parser.xmsgs
   * ila_test.gise
   * ila_test.xise

Deliver Readme:
   Readme file for the IP.

   * ila_test_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * ila_test_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

