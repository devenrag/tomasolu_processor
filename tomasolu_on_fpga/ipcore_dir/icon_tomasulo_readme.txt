The following files were generated for 'icon_tomasulo' in directory
C:\Xilinx_projects\tomasolu\tomasolu\ipcore_dir\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * icon_tomasulo.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * icon_tomasulo.constraints/icon_tomasulo.ucf
   * icon_tomasulo.constraints/icon_tomasulo.xdc
   * icon_tomasulo.ngc
   * icon_tomasulo.ucf
   * icon_tomasulo.vhd
   * icon_tomasulo.vho
   * icon_tomasulo.xdc
   * icon_tomasulo_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * icon_tomasulo.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * icon_tomasulo.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * icon_tomasulo.gise
   * icon_tomasulo.xise

Deliver Readme:
   Readme file for the IP.

   * icon_tomasulo_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * icon_tomasulo_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

