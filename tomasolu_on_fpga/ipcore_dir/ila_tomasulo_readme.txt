The following files were generated for 'ila_tomasulo' in directory
C:\Xilinx_projects\tomasolu\tomasolu\ipcore_dir\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * ila_tomasulo.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * ila_tomasulo.cdc
   * ila_tomasulo.constraints/ila_tomasulo.ucf
   * ila_tomasulo.constraints/ila_tomasulo.xdc
   * ila_tomasulo.ncf
   * ila_tomasulo.ngc
   * ila_tomasulo.ucf
   * ila_tomasulo.vhd
   * ila_tomasulo.vho
   * ila_tomasulo.xdc
   * ila_tomasulo_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * ila_tomasulo.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * ila_tomasulo.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * _xmsgs/pn_parser.xmsgs
   * ila_tomasulo.gise
   * ila_tomasulo.xise

Deliver Readme:
   Readme file for the IP.

   * ila_tomasulo_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * ila_tomasulo_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

