The following files were generated for 'ila_inst' in directory
C:\Xilinx_projects\tomasolu\tomasolu\ipcore_dir\

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * ila_inst.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * ila_inst.cdc
   * ila_inst.constraints/ila_inst.ucf
   * ila_inst.constraints/ila_inst.xdc
   * ila_inst.ncf
   * ila_inst.ngc
   * ila_inst.ucf
   * ila_inst.vhd
   * ila_inst.vho
   * ila_inst.xdc
   * ila_inst_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * ila_inst.asy

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * ila_inst.sym

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * _xmsgs/pn_parser.xmsgs
   * ila_inst.gise
   * ila_inst.xise

Deliver Readme:
   Readme file for the IP.

   * ila_inst_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * ila_inst_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

